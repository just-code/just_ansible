# Just an ansible script

We just have automated the installation of [contact backend](https://gitlab.com/just-code/just_contact_backend/). This setup was tested with debian 10, but should work well with ubuntu. If you support for a different OS create a [new issue](https://gitlab.com/just-code/just_ansible/-/issues). This is [MIT licensed](https://gitlab.com/just-code/just_ansible/-/blob/master/LICENSE) code, use it for your commercial and open source projects and when you enhance it we are happy to receive a pull request. 

## Local setup

Ansible automates your remote server installations. Here is a [short video how](https://www.ansible.com/resources/videos/quick-start-video). To run make sure you have ansible installed on your machine. Follow [this guide](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) to get ansible.

You need also the necessary ansible-galaxy roles to install locally.

```bash
ansible-galaxy install nginxinc.nginx
ansible-galaxy install geerlingguy.certbot
ansible-galaxy install geerlingguy.php
```

Copy and edit the configuration.yml file:

```bash
cp playbooks/vars/configuration-example.yml playbooks/vars/configuration.yml
```

1. Set *shared_secret* to match the value of your frontend
1. Set [your google recaptcha](https://www.google.com/recaptcha/admin) value *recaptcha_secret*.
1. Set *mail.to* value that will be used to receive mails.
1. Set *frontend.url* **without** the trailing slash.
1. Set *dns_api_file* and *dns_provider* if you want use wildcard https certs and specify them in *certbot.domains*.

## Deploy
We run the playbook as privileged user. If your local and remote users differ provide it with *-u* flag.

```bash
ansible-playbook playbooks/just_backend.yml -i playbooks/hosts-example.yml
```
This playbook will run on [just-code.cloud](https://just-code.cloud). Be sure to adjust the hosts-example.yml file to match your hosts

## Admire

Give yourself a pat on the back and admire the great work of our ansible script [on the most awesome backend](https://just-code.cloud).